package com.JPetStore.Test;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.JPetStore.base.BaseClass;

public class InvalidLoginTest extends BaseClass {
	@DataProvider(name="Login data")
	public Object[][] getLoginData(){
		return new Object[][] {
			{"Suryansh","Surya123"}
		};
	}
	@Test(dataProvider = "Login data")
	public void  loggedin(String username, String password) throws Exception {
		invalidLogin.Invalidlogging(username, password);
	}
}



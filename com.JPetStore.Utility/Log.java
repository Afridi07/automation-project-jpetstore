package com.JPetStore.Utility;

import org.apache.log4j.Logger;

public class Log {
	//initialized log4j logs
	//public static Logger log = Logger.getLogger("Log");
	public static Logger log = Logger.getLogger("Log");
	public static void startTestCase(String TestCaseName) {
	log.info("================"+TestCaseName+"\nTest Start===============");
	}

 
	public static void endTestCase(String TestCaseName) {
	log.info("================"+TestCaseName+"\nTest End ===============");
	}
	//creat below method to call ehenever it is required
	public static  void info(String message) {
		log.info(message);
	}
	public static void warn(String message) {
		log.warn(message);
	}
	public static void error(String message) {
		log.error(message);
	}
	public static void fatal(String message) {
		log.fatal(message);
	}
}


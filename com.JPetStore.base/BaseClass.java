package com.JPetStore.base;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.xml.DOMConfigurator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.JPetStore.Pages.BillPage;
import com.JPetStore.Pages.BirdsOrderPage;
import com.JPetStore.Pages.CatOrderPage;
import com.JPetStore.Pages.DogOrderPage;
import com.JPetStore.Pages.FishOrderPage;
import com.JPetStore.Pages.InvalidLogin;
import com.JPetStore.Pages.LoginPage;
import com.JPetStore.Pages.RegisterationPage;
import com.JPetStore.Pages.ReptilesOrderPage;
import com.JPetStore.Utility.*;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class BaseClass {

	public static WebDriver driver;
	public static RegisterationPage registerpage;
	public static LoginPage lognpge;
	public static ReptilesOrderPage rptlOrdr;
	public static BirdsOrderPage birds;
	public static FishOrderPage fish;
	public static DogOrderPage dog;
	public static CatOrderPage cat;
	public static BillPage bill;
	public static InvalidLogin invalidLogin;
	public static ExtentSparkReporter esr;
	public static ExtentReports er;
	public static ExtentTest et;

	@BeforeSuite
	public void beforeSuite() {
		DOMConfigurator.configure("log4j.xml");
		Log.info("This is before Suite");
		String path = "C:\\Users\\Abdul\\eclipse-workspace\\automation_by_team6\\test-output\\Extent Report\\Report.html";
		esr = new ExtentSparkReporter(path);
		esr.config().setDocumentTitle("Automation Report");
		esr.config().setReportName("JPetStore");
		esr.config().setTheme(Theme.DARK);
		er = new ExtentReports();
		er.attachReporter(esr);
		er.setSystemInfo("HostName", "Abdul Afridi");
		er.setSystemInfo("Username", "Afridi07");
		er.setSystemInfo("OS", "Windows");
		er.setSystemInfo("Window", "Windows-11");
		er.setSystemInfo("Environment", "QA");
		er.setSystemInfo("Date", "13/12/2023");

		et = er.createTest("myfirsttest", "searching for automation");
	}
	@Test
	public void abd() {
		
	}

	@BeforeMethod
	public void setUp() {
		// Setting the property to communicate with the Brwoser
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\Abdul\\eclipse-workspace\\automation_by_team6\\Driver\\chromedriver.exe");
		// Intiating chrome driver
		driver = new ChromeDriver();
		// Maximizing the Window
		driver.manage().window().maximize();
		// Going to particular site of Jpetstore
		driver.get("https://petstore.octoperf.com/actions/Catalog.action");
		et=er.createTest("JPetstore");
		// Waiting to load all the elements of web page
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		// Object of register page
		registerpage = new RegisterationPage(driver);
		// object of login page
		lognpge = new LoginPage(driver);
		// Object of reptile order page
		rptlOrdr = new ReptilesOrderPage(driver);
		// Object of birds order page
		birds = new BirdsOrderPage(driver);
		// Object of fish order page
		fish = new FishOrderPage(driver);
		// Object of dog order page
		dog = new DogOrderPage(driver);
		// Object of cat order page
		cat = new CatOrderPage(driver);
		// Object of Bill page
		bill = new BillPage(driver);
		// Object for Invalid Login Page
		invalidLogin = new InvalidLogin(driver);
		
	}

	@AfterMethod
	public void tearDown() {
		er.flush();
		// To close the web browser
		driver.quit();
	}

}
package com.JPetStore.Pages;
 
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.JPetStore.Utility.Log;
import com.JPetStore.base.BaseClass;
import com.aventstack.extentreports.Status;
 
public class ReptilesOrderPage extends BaseClass {
 
	//Using the page factory model
	public ReptilesOrderPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
 
	}
	//Locating the Reptile option on homepage
	@FindBy(xpath="//*[@id=\"SidebarContent\"]/a[4]/img")
	WebElement clkReptile;
	//Locating Rattlesnake option
	@FindBy(xpath="//*[@id=\"Catalog\"]/table/tbody/tr[2]/td[1]/a")
	WebElement rattlesnake;
	//Locating add to cart option
	@FindBy(xpath="//*[@id=\"Catalog\"]/table/tbody/tr[2]/td[5]/a")
	WebElement addVenomless;
	//Locating Update option
	@FindBy(name="updateCartQuantities")
	WebElement updtCart2;
	@FindBy(xpath="//*[@id=\"BackLink\"]/a")
	//Locating Returbnn to menu Option
	WebElement retrnMenu2;
	@FindBy(xpath="//*[@id=\"SidebarContent\"]/a[4]/img")
	//Locating reptile option
	WebElement clkReptile2;
	// Locating Iguana Option 
	@FindBy(xpath="//*[@id=\"Catalog\"]/table/tbody/tr[3]/td[1]/a")
	WebElement Iguana;
	//Locating add to cart option
	@FindBy(xpath="//*[@id=\"Catalog\"]/table/tbody/tr[2]/td[5]/a")
	WebElement addIguana;
	//locating Update cart Option
	@FindBy(name="updateCartQuantities")
	WebElement updtCart;
	//Locating The return menu option
	@FindBy(xpath="//*[@id=\"BackLink\"]/a")
	WebElement retrnMenu;
	//Testing the order reptile module
	public BirdsOrderPage orderReptile() {
		et.log(Status.INFO, "Testing the Reptiles Order Test Module");
		Log.startTestCase("Testing the reptiles order module");
		Log.info("Clicking on Reptile option");
		clkReptile.click();
		Log.info("Clicking on return to menu to return on home page of the website");
		String crtUrl = driver.getCurrentUrl();  
		Log.info("Original Url of the module");
		String OrgUrl ="https://petstore.octoperf.com/actions/Catalog.action?viewCategory=&categoryId=REPTILES"; 
		Log.info("Validating the URl of the module by using Assert");
		Assert.assertEquals(crtUrl, OrgUrl);                                                                  
		Log.info("URL Is matched");
		Log.info("Clicking on Rattle snake Option");                                                                                
		rattlesnake.click();
		Log.info("Clicking on Add to cart Option");
		addVenomless.click();
		
		Log.info("Clicking on update to update the cart");
		updtCart2.click();
		retrnMenu2.click();
		clkReptile2.click();
		Log.info("Clicking on Igguana Option");        
		Iguana.click();
		Log.info("Clicking on Add to cart Option");
		addIguana.click();
		
		Log.info("Clicking on update to update the cart");
		updtCart.click();
		Log.info("Clicking on return to menu to return on home page of the website");
		retrnMenu.click();
		Log.info("Reptiles order Module is complete here");
		et.log(Status.INFO, "Completed Reptiles Order test here");
		et.log(Status.INFO, "Passed Test Case");
		//return to Birds Order Page 
		return new BirdsOrderPage(BaseClass.driver);
	}


}


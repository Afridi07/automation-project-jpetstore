package com.JPetStore.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.JPetStore.Utility.Log;
import com.JPetStore.base.BaseClass;
import com.aventstack.extentreports.Status;

public class BirdsOrderPage extends BaseClass {
	// Locating Bird Option
	@FindBy(xpath = "//*[@id=\"SidebarContent\"]/a[5]/img")
	WebElement Bird;
	// Locating Amazon Parrot Option
	@FindBy(xpath = "//*[@id=\"Catalog\"]/table/tbody/tr[2]/td[1]/a")
	WebElement AmznPrt;
	// Locating Add To Cart Option
	@FindBy(xpath = "//*[@id=\"Catalog\"]/table/tbody/tr[2]/td[5]/a")
	WebElement addCart;

	// Locationg Finch Option
	@FindBy(xpath = "//*[@id=\"Catalog\"]/table/tbody/tr[3]/td[1]/a")
	WebElement Finch;

	// Locating Return to menu option
	@FindBy(xpath = "//*[@id=\"BackLink\"]/a")
	WebElement retrnMenu2;
	// Locating Update option
	@FindBy(xpath = "//*[@name=\"updateCartQuantities\"]")
	WebElement Updt;

	// Using page factory model

	public BirdsOrderPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	public BillPage orderBird() {
		et.log(Status.INFO, "Testing the Bird Order Module");
		Log.startTestCase("Testing The Bird Order Module");

		Log.info("Clicking On Bird Option");
		Bird.click();
		Log.info("Gertting Current URL of the website");
		String crtUrl = driver.getCurrentUrl();
		Log.info("Original Url Of the Mudule");
		String OrgUrl = "https://petstore.octoperf.com/actions/Catalog.action?viewCategory=&categoryId=BIRDS";
		Log.info("Validating the URL by using Assert ");
		Assert.assertEquals(crtUrl, OrgUrl);
		Log.info("Url Is Matched");
		Log.info("Clicking On Amazon Parrot Link to order That");
		AmznPrt.click();
		Log.info("Clicking on Add to cart Option");
		addCart.click();
		Log.info("Clicking on update to update the cart");
		Updt.click();
		Log.info("Clicking on return to menu to return on home page of the website");
		retrnMenu2.click();
		Log.info("Again Clicking on Bird to Order");
		Bird.click();
		Log.info("Clicking on finch Bird Option");
		Finch.click();
		Log.info("Clicking on Add to cart Option");
		addCart.click();
		Log.info("Clicking on update to update the cart");
		Updt.click();
		Log.info("Clicking on return to menu to return on home page of the website");
		retrnMenu2.click();
		Log.info("Bird Module is Completed Here");
		et.log(Status.INFO, "Completed Bird order test here");
		et.log(Status.INFO, "Passed Test Case");
		// Returning to BillPage class
		return new BillPage(BaseClass.driver);

	}

}

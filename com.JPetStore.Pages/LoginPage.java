
package com.JPetStore.Pages;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import com.JPetStore.Utility.Log;
import com.JPetStore.base.BaseClass;
import com.aventstack.extentreports.Status;

public class LoginPage extends BaseClass {

	// Locating Signin Button
	@FindBy(xpath = "//*[@id=\"MenuContent\"]/a[2]")
	WebElement SigninBtn;
	// Locating Username TextBox
	@FindBy(xpath = "//*[@name=\"username\"]")
	WebElement user;
	// Locating Password TextBox
	@FindBy(xpath = "//*[@id=\"Catalog\"]/form/p[2]/input[2]")
	WebElement pass;
	// Locating Login Button
	@FindBy(xpath = "//*[@id=\"Catalog\"]/form/input")
	WebElement login;

	// Using Page factory Model
	public LoginPage(WebDriver driver) {
		PageFactory.initElements(driver, this);

	}

	public FishOrderPage logging() throws Exception {
		et.log(Status.INFO, "Testing the Login Module");
		Log.startTestCase("Testing The Login Class");
		// Giving the file location
		FileInputStream fis = new FileInputStream(
				"C:\\Users\\Abdul\\eclipse-workspace\\automation_by_team6\\src\\test\\resources\\Automation Data.xlsx");
		// Creating A workbook
		XSSFWorkbook workBook = new XSSFWorkbook(fis);
		// Crating a worksheet
		XSSFSheet worksheet = workBook.getSheetAt(0);
		// Getting row numbers
		int rows = worksheet.getLastRowNum();
		int columns = worksheet.getRow(1).getLastCellNum();

		// Using for loop retreiving the data from excel file
		for (int r = 1; r < rows; r++) {
			XSSFRow row = worksheet.getRow(r);

			Log.info("Clicking On Sig In Button");
			SigninBtn.click();
			Log.info("Enterting the data from excel sheet to Username textbox");
			user.sendKeys(row.getCell(0).getStringCellValue());
			Log.info("Clearing the password textbox");
			pass.clear();
			Log.info("Enterting the data from excel sheet to Username textbox");
			pass.sendKeys(row.getCell(1).getStringCellValue());
			Log.info("Clicking On LogIn Button");
			login.click();
			Log.info("Here Login Module test Is completed");
			et.log(Status.INFO, "Completed Login test here");
			et.log(Status.INFO, "Passed Test Case");
		}
		// RETURNING TO THE FISH ORDER PAGE
		return new FishOrderPage(BaseClass.driver);

	}
}

package com.JPetStore.Pages;

import com.JPetStore.Utility.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.apache.commons.io.FileUtils;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;
import com.JPetStore.base.BaseClass;
import com.aventstack.extentreports.Status;

import org.apache.log4j.xml.DOMConfigurator;

public class InvalidLogin extends BaseClass {

	@FindBy(xpath = "//*[@id=\"MenuContent\"]/a[2]")
	WebElement SigninBtn;

	@FindBy(xpath = "//*[@name=\"username\"]")
	WebElement user;
	@FindBy(xpath = "//*[@id=\"Catalog\"]/form/p[2]/input[2]")
	WebElement pass;
	@FindBy(xpath = "//*[@id=\"Catalog\"]/form/input")
	WebElement login;

	public InvalidLogin(WebDriver driver) {
		PageFactory.initElements(driver, this);

	}

	public FishOrderPage Invalidlogging(String username, String password) throws Exception {
		et.log(Status.INFO, "Testing the Invalid Login Module");
		Log.startTestCase("Testing With Invalid Login Data");
		Log.info("Clicking On SigIn Button");
		SigninBtn.click();
		Log.info("Getting Username from excel File and Pastint in Usename Textbox");
		user.sendKeys(username);
		Log.info("Clearing the password TextBox");
		pass.clear();
		Log.info("Getting password from excel file and pasting in Password Textbox");
		pass.sendKeys(password);
		Log.info("Clicking on logIn Button");
		login.click();
		Log.info("Taking Screenshot of failed Result");
		TakesScreenshot ts = (TakesScreenshot) driver;
		Log.info("Storing into a file");
		File src = ts.getScreenshotAs(OutputType.FILE);
		File trg = new File(".//screenshots//homepage.jpeg");
		FileUtils.copyFile(src, trg);
		et.log(Status.INFO, "Completed Invalid Login test here");
		et.log(Status.INFO, "Passed Test Case");

		return new FishOrderPage(BaseClass.driver);

	}
}
package com.JPetStore.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.JPetStore.Utility.Log;
import com.JPetStore.base.BaseClass;
import com.aventstack.extentreports.Status;

public class DogOrderPage extends BaseClass {
	// Locating The dog option in homepage
	@FindBy(xpath = "//*[@alt=\"Dogs\"]")
	WebElement Dog;
	// Locating the Bull DOg Option
	@FindBy(xpath = "//*[@id=\"Catalog\"]/table/tbody/tr[2]/td[1]/a")
	WebElement Bulldog;
	// Locationg the Poofdle Dog Option
	@FindBy(xpath = "//*[@id=\"Catalog\"]/table/tbody/tr[3]/td[1]/a")
	WebElement Poodle;
	// Locating the Dalmation dog option
	@FindBy(xpath = "//*[@id=\"Catalog\"]/table/tbody/tr[4]/td[1]/a")
	WebElement Dalmation;
	// Locating GoldenRetriver Option
	@FindBy(xpath = "//*[@id=\"Catalog\"]/table/tbody/tr[5]/td[1]/a")
	WebElement GoldenRetriever;
	// Location the Labrador Dog option
	@FindBy(xpath = "//*[@id=\"Catalog\"]/table/tbody/tr[6]/td[1]/a")
	WebElement LabradorRetriever;
	// Locating the Chihauhua dog option
	@FindBy(xpath = "//*[@id=\"Catalog\"]/table/tbody/tr[7]/td[1]/a")
	WebElement Chihuahua;
	// Locating add to cart option
	@FindBy(xpath = "//*[@id=\"Catalog\"]/table/tbody/tr[2]/td[5]/a")
	WebElement addCart;
	// Locationg Update option
	@FindBy(xpath = "//*[@name=\"updateCartQuantities\"]")
	WebElement Updt;
	// Location Return to menu Option
	@FindBy(xpath = "//*[@id=\"BackLink\"]/a")
	WebElement Return;

	// Using page Factory model
	public DogOrderPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
// Testing the Dog Order module
	public CatOrderPage orderDog() {
		et.log(Status.INFO, "Testing the Dog Order Module");
		Log.startTestCase("Testing the Dog Order Module");
		Log.info("Clicking on Dog Option of the homepage");
		Dog.click();
		Log.info("Getting the curerent Url of the website");
		String crtUrl = driver.getCurrentUrl();
		Log.info("Original Url of the module");
		String OrgUrl = "https://petstore.octoperf.com/actions/Catalog.action?viewCategory=&categoryId=DOGS";
		Log.info("Validating the URl of the module by using Assert");
		Assert.assertEquals(crtUrl, OrgUrl);
		Log.info("URL Is matched");
		Log.info("Clicking on Bull Dog Option");
		Bulldog.click();
		Log.info("Clicking on Add to cart Option");
		addCart.click();
		Log.info("Clicking on update to update the cart");
		Updt.click();
		Log.info("Clicking on return to menu to return on home page of the website");
		Return.click();
		Log.info("Clicking on Dog Option of the homepage");
		Dog.click();
		Log.info("Clicking on Poodle Dog Option");
		Poodle.click();
		Log.info("Clicking on Add to cart Option");
		addCart.click();
		Log.info("Clicking on update to update the cart");
		Updt.click();
		Log.info("Clicking on return to menu to return on home page of the website");
		Return.click();
		Log.info("Clicking on Dog Option of the homepage");
		Dog.click();
		Log.info("Clicking on Dalmation Dog Option");
		Dalmation.click();
		Log.info("Clicking on Add to cart Option");
		addCart.click();
		Log.info("Clicking on update to update the cart");
		Updt.click();
		Log.info("Clicking on return to menu to return on home page of the website");
		Return.click();
		Log.info("Clicking on Dog Option of the homepage");
		Dog.click();
		Log.info("Clicking on Golden Retreiver Dog Option");
		GoldenRetriever.click();
		Log.info("Clicking on Add to cart Option");
		addCart.click();
		Log.info("Clicking on update to update the cart");
		Updt.click();
		Log.info("Clicking on return to menu to return on home page of the website");
		Return.click();
		Log.info("Clicking on Dog Option of the homepage");
		Dog.click();
		Log.info("Clicking on Labrador Dog Option");
		LabradorRetriever.click();
		Log.info("Clicking on Add to cart Option");
		addCart.click();

		Log.info("Clicking on update to update the cart");
		Updt.click();
		Log.info("Clicking on return to menu to return on home page of the website");
		Return.click();
		Log.info("Clicking on Dog Option of the homepage");
		Dog.click();
		Log.info("Clicking on Chihuahua Dog Option");
		Chihuahua.click();
		Log.info("Clicking on Add to cart Option");
		addCart.click();
		Log.info("Clicking on update to update the cart");
		Updt.click();
		Log.info("Clicking on return to menu to return on home page of the website");
		Return.click();
		Log.info("Dog Module test is complete here");
		et.log(Status.INFO, "Completed Dog order test here");
		et.log(Status.INFO, "Passed Test Case");
		
		//Returning to the cat order page
		return new CatOrderPage(BaseClass.driver);
	}
}


package com.JPetStore.Pages;

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.FindBy;

import org.openqa.selenium.support.PageFactory;

import com.JPetStore.Utility.Log;
import com.JPetStore.base.BaseClass;
import com.aventstack.extentreports.Status;

public class RegisterationPage extends BaseClass {
	// Locating the Sigin Button
	@FindBy(xpath = "//*[@id=\"MenuContent\"]/a[2]")

	WebElement SigninBtn;
	// Locating the register option
	@FindBy(xpath = "//*[@id=\"Catalog\"]/a")

	WebElement RegisterClk;
	// Locating the username textbox
	@FindBy(name = "username")

	WebElement usrname;
	// locating the password textbox
	@FindBy(xpath = "//*[@name=\"password\"]")

	WebElement passwrd;
	// Locating the repeat password textbox
	@FindBy(xpath = "//*[@name=\"repeatedPassword\"]")

	WebElement repasswrd;
	// Locating the firsname textbox
	@FindBy(name = "account.firstName")

	WebElement frstname;
	// Locating the last Name textbox
	@FindBy(name = "account.lastName")

	WebElement lstname;
	// Locating the email textbox
	@FindBy(name = "account.email")

	WebElement email;
	// Locating the phome no textbox
	@FindBy(name = "account.phone")

	WebElement phnno;
	// Locating the address Line 1 textbox
	@FindBy(name = "account.address1")

	WebElement add1;
	// Locating the address Line 2 textbox
	@FindBy(name = "account.address2")

	WebElement add2;
	// Locating the city name textbox
	@FindBy(name = "account.city")

	WebElement city;
	// Locating the State textbox
	@FindBy(name = "account.state")

	WebElement state;
	// Locating the zipcode textbox
	@FindBy(name = "account.zip")

	WebElement zipcode;
	// Locating the country textbox
	@FindBy(name = "account.country")

	WebElement country;
	// Locating the submit option
	@FindBy(name = "newAccount")

	WebElement submt;

	// using the Page factory model
	public RegisterationPage(WebDriver driver) {

		PageFactory.initElements(driver, this);

	}

	public LoginPage loginInput() {
		et.log(Status.INFO, "Testing the Registration Module");
		Log.startTestCase("Testing the Regiter module");
		Log.info("Clicking on signin Button");
		SigninBtn.click();
		Log.info("Clicking on register button");
		RegisterClk.click();
		Log.info("Sending the Usename as Aastha in username textbox");
		usrname.sendKeys("Aastha");

		passwrd.sendKeys("Team6");
		Log.info("Sending the Password as Team6 in RepeatPassword Textbox");
		repasswrd.sendKeys("Team6");
		Log.info("Sending the first name as Aastha");
		frstname.sendKeys("Aastha");
		Log.info("Sending the last name as Kumari");
		lstname.sendKeys("kumari");
		Log.info("Sending the email as aastha123@gmail.com");
		email.sendKeys("aastha123@gmail.com");
		Log.info("Sending the phone no as 893678213");
		phnno.sendKeys("8903678213");
		Log.info("Sending the Data as Telco Colony in Address Line 1 Textbox");
		add1.sendKeys("Telco colony");
		Log.info("Sending the Data as House no-23 in Address line 2 Textbox");
		add2.sendKeys("House no-23");
		Log.info("Sending the data as Jamshedpur in city Textbox");
		city.sendKeys("Jamshedpur");
		Log.info("Sending the Data as Jharkhand in State Textbox");
		state.sendKeys("Jharkhand");
		Log.info("Sending the value as 867881 in Zipcode Textbox");
		zipcode.sendKeys("867881");
		Log.info("Sending the Country as India in Country Textbox");
		country.sendKeys("India");
		Log.info("Clicking on submit button");
		submt.click();
		et.log(Status.INFO, "Completed Registration test here");
		et.log(Status.INFO, "Passed Test Case");

		return new LoginPage(BaseClass.driver);

	}

}
package com.JPetStore.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.JPetStore.Utility.Log;
import com.JPetStore.base.BaseClass;
import com.aventstack.extentreports.Status;

public class FishOrderPage extends BaseClass {
	//Locating the fish option from home page
	@FindBy(xpath="//*[@id=\"QuickLinks\"]/a[1]")
	WebElement clkFish;
	// Location g AFish Option
	@FindBy(xpath="//*[@id=\"Catalog\"]/table/tbody/tr[2]/td[1]/a")
	WebElement AFish;
	//Locating Add to carT Option
	@FindBy(xpath = "//*[@id=\"Catalog\"]/table/tbody/tr[2]/td[5]/a")
	WebElement AddCart;
	//Locating Update option
	@FindBy(xpath = "//*[@name=\"updateCartQuantities\"]")
	WebElement Update;
	//Locating Add to cart Option
	@FindBy(xpath = "//*[@id=\"Catalog\"]/table/tbody/tr[3]/td[5]/a")
	WebElement AddCart2;
	//Locating Tigershark Option
	@FindBy(xpath="//*[@id=\"Catalog\"]/table/tbody/tr[3]/td[1]/a")
	WebElement TigerShark;
	//Locating Koi Option
	@FindBy(xpath="//*[@id=\"Catalog\"]/table/tbody/tr[4]/td[1]/a")
	WebElement koi;
	//Locating GoldenFish option
	@FindBy(xpath = "//*[@id=\"Catalog\"]/table/tbody/tr[5]/td[1]/a")
	WebElement GoldenFish;
	//Locating Return To Menu option;
	@FindBy(xpath="//*[@id=\"BackLink\"]/a")
	WebElement retrnMenu2;
	//Using Page factory model Here
	public FishOrderPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	//testing the Fish Order Module
	public DogOrderPage orderFish() {
		et.log(Status.INFO, "Testing the Fish Order Module");
		Log.startTestCase("Testing the Fish Order Module");
		Log.info("Clicking on Fish Option of the homepage");
		clkFish.click();
		Log.info("Getting the curerent Url of the website");
		String crtUrl = driver.getCurrentUrl();
		Log.info("Original Url of the module");
		String OrgUrl ="https://petstore.octoperf.com/actions/Catalog.action?viewCategory=&categoryId=FISH";
		Log.info("Validating the URl of the module by using Assert");
	   Assert.assertEquals(crtUrl, OrgUrl);
	   Log.info("URL Is matched");
		Log.info("Clicking on A-FISH Option");
		AFish.click();
		Log.info("Clicking on Add to cart Option");
		AddCart.click();
		Log.info("Clicking on update to update the cart");
		Update.click();
		Log.info("Clicking on Fish Option of the homepage");
		clkFish.click();
		Log.info("Clicking on A-FISH Option");
		AFish.click();
		Log.info("Clicking on Add to cart Option");
		AddCart2.click();
		Log.info("Clicking on update to update the cart");
		Update.click();
		Log.info("Clicking on Fish Option of the homepage");
		clkFish.click();
		Log.info("Clicking on Tiger Shark Option");
		TigerShark.click();
		Log.info("Clicking on Add to cart Option");
		AddCart.click();
		Log.info("Clicking on update to update the cart");
		Update.click();
		Log.info("Clicking on Fish Option of the homepage");
		clkFish.click();
		Log.info("Clicking on KOI-FISH Option");
		koi.click();
		Log.info("Clicking on Add to cart Option");
		AddCart.click();
		Log.info("Clicking on update to update the cart");
		Update.click();
		Log.info("Clicking on Fish Option of the homepage");
		clkFish.click();
		Log.info("Clicking on KOI-FISH Option");
		koi.click();
		Log.info("Clicking on Add to cart Option");
		AddCart2.click();
		Log.info("Clicking on update to update the cart");
		Update.click();
		Log.info("Clicking on Fish Option of the homepage");
		clkFish.click();
		Log.info("Clicking on Golden FISH Option");
		GoldenFish.click();
		Log.info("Clicking on Add to cart Option");
		AddCart.click();
		Log.info("Clicking on update to update the cart");
		Update.click();
		Log.info("Clicking on Fish Option of the homepage");
		clkFish.click();
		Log.info("Clicking on Golden FISH Option");
		GoldenFish.click();
		Log.info("Clicking on Add to cart Option");
		AddCart2.click();
		Log.info("Clicking on update to update the cart");
		Update.click();
		Log.info("Clicking on return to menu to return on home page of the website");
		retrnMenu2.click();
		Log.info("Fish Order Module Is completed Here");
		et.log(Status.INFO, "Completed Fish order test here");
		et.log(Status.INFO, "Passed Test Case");
		//Returning to the Dog Order page
		return new DogOrderPage(BaseClass.driver);
		
	}


}

package com.JPetStore.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.JPetStore.Utility.Log;
import com.JPetStore.base.BaseClass;
import com.aventstack.extentreports.Status;

public class CatOrderPage extends BaseClass {
	//Locating Cat option
	@FindBy(xpath = "//*[@id=\"QuickLinks\"]/a[4]")
	WebElement ClkCat;
	//Locating Manx Cat option
	@FindBy (xpath = "//*[@id=\"Catalog\"]/table/tbody/tr[2]/td[1]/a")
	WebElement Manx;
	//Locating Persian cat Option
	@FindBy(xpath = "//*[@id=\"Catalog\"]/table/tbody/tr[3]/td[1]/a")
	WebElement Persian;
	//Locating Add to cart Option
	@FindBy(xpath = "//*[@id=\"Catalog\"]/table/tbody/tr[2]/td[5]/a")
	WebElement AddCart1;
	//Locating Add to cart Option
	@FindBy(xpath = "//*[@id=\"Catalog\"]/table/tbody/tr[3]/td[5]/a")
	WebElement addCart2;
	//locating Update option to update cart
	@FindBy(xpath = "//*[@name=\"updateCartQuantities\"]")
	WebElement Updt;
	//Locating return to menu option 
	@FindBy(xpath = "//*[@id=\"BackLink\"]/a")
	WebElement Return;
	

//Using Page factory Model 
public CatOrderPage(WebDriver driver) {
	PageFactory.initElements(driver, this);
}

public ReptilesOrderPage orderCat() {
	et.log(Status.INFO, "Testing the Cat Order Module");
	Log.startTestCase("Testing Cat Order Module");
	Log.info("Clicking on cat Option");
	ClkCat.click();
	Log.info("Getting the curerent Url of the website");
	String crtUrl = driver.getCurrentUrl();
	Log.info("Original Url of the module");
	 String OrgUrl ="https://petstore.octoperf.com/actions/Catalog.action?viewCategory=&categoryId=CATS";
	 Log.info("Validating the URl of the module by using Assert" );
    Assert.assertEquals(crtUrl, OrgUrl);
    Log.info("URL Is matched");
    Log.info("Clicking on Manx Cat Option");
	Manx.click();
	Log.info("Clicking on Add to cart Option");
	AddCart1.click();
	Log.info("Clicking on update to update the cart");
	Updt.click();
	Log.info("Clicking on cat Option");
	ClkCat.click();
    Log.info("Clicking on Manx Cat Option");
	Manx.click();
	Log.info("Clicking on Add to cart Option");
	addCart2.click();
	Log.info("Clicking on update to update the cart");
	Updt.click();
	Log.info("Clicking on cat Option");
	ClkCat.click();
	Log.info("Clicking on Persian Cat Option");
	Persian.click();
	Log.info("Clicking on Add to cart Option");
	AddCart1.click();
	Log.info("Clicking on update to update the cart");
	Updt.click();
	Log.info("Clicking on cat Option");
	ClkCat.click();
	Log.info("Clicking on Persian Cat Option");
	Persian.click();
	Log.info("Clicking on Add to cart Option");
	addCart2.click();
	Log.info("Clicking on update to update the cart");
	Updt.click();
	Log.info("Clicking on return to menu to return on home page of the website");
	Return.click();
	Log.info("Cat Module is completed Here");	
	et.log(Status.INFO, "Completed Cat order test here");
	et.log(Status.INFO, "Passed Test Case");
	//Returning to the reptile order page
	return new ReptilesOrderPage(BaseClass.driver);
}
}
